---
title: "The Joke Is on Me"
date: 2022-08-20T11:19:14-04:00
draft: false
---

Hahahahahahahahahahahah

Who am I kidding? I'm still scare of putting myself out there on the internet. I know it's unreasonable. I do a lot of that kind of stuff. Stuff of the unreasonable nature. The ideas seem bigger than me and that's because they are. Since I tend to treat them that way, I don't want to drunkly stumble into them. Not sure why I treat them as if they are fragile and precious. Maybe because I was taught that ideas can change the world. You would think at my age I would realize that is not the full picture. But here I am, posting more _reasons_ why I haven't taken up writing in a public forum. 
