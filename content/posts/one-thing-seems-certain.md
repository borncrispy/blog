---
title: "One Thing Seems Certain"
date: 2022-07-13T17:24:13-04:00
draft: true
---
You'd be hard pressed to look at a newspaper, a magazine, the news, or social media and not get the impression that the US is falling apart. Depending on whom you ask the answers will vary. It seems most of the focus is on those differences. Underneath it all is fear. However, and more importantly, at the center of the fear is a lack of understanding about what the future holds. 

Although a piece in the New York Times wrote that Republican are more positive about the future it seems that this future and their happiness is hinging on one important detail: Democrats are voted out. This is something I find impossible to see happening any time in the future. All the twisting and turning of the political system will never remove all Democrats without conflict on a scale we haven't seen in this country for some time. 

