---
title: "The Weird Thing About Perfectionism and Procrastination"
date: 2022-06-14T13:37:05-04:00
draft: false
---
I have been avoiding writing posts for my personal site that I set up for myself back in 2015 because I was afraid. That same fear will likely show up here. I feel that I currently leave a lot to be desire in my writing. I also run into a lot of challenges with communicating and organizing my thoughts due to struggles with ADHD. "Really interesting ideas, but needs better organization" was the statement made about 99.9% of my papers in college. I would often give up before I ever started. This fear of failure has consistently plagued all my creative endeavors despite how much I enjoy getting lost in the process. My perfectionism made the process unbearable and procrastination took root. 

Recently I read [this article on Fast Company](https://www.fastcompany.com/90390856/these-3-weird-tricks-have-helped-me-beat-work-procrastination). Let me just be clear, I hate productivity hacks or life hacks in general. I want to be able to do something that has no value to anyone but me and it has been my experience that in American culture this is a childish desire. However the first of the three tips in the article really stuck out to me. I have been thinking about it ever since. It just keeps rolling around in my head. I feel like the only way to stop thinking about it is to take action on it.

Prepare yourself 2022 for the worst on full display here.
