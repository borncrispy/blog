---
title: "About"
featured_image: "img/chips-header.jpg"
date: 2022-05-13T13:40:46-04:00
menu:
  main:
    weight: 1
draft: false
---
{{< figure src="../img/Lincoln-2.png" >}}

My name is Lincoln. I have never been a good writer. I'd like to change that.
